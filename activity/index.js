const express = require('express');

const app = express(); 

const port = 3000;

app.use(express.json())

	app.use(express.urlencoded({extended:true}));

// to get welcome home page
app.get('/home', (req, res) => {
	
	res.send("Welcome to the Home Page");
})



// to input data in a mock database
let users = [];

app.post('/user-input', (req, res) => {

	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){

		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered`);
	} else {

		res.send('Please input BOTH username and Password')
	}
});


// to update users
app.put('/user-update', (req, res) => {


	let message;


	for(let i = 0; i < users.length; i++){

		if (req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has updated.`;

			break;


		} else {

			message = "user does not exist.";
		}
	}

	res.send(message);

})

// to get the latest user database
app.get('/users', (req, res) => {
	res.json(users);
})


app.delete('/delete-user', (req, res) => {

	console.log(req.body);

	if(req.body.username !== ''){

		users.pop(req.body);

		res.send(`User ${req.body.username} successfully deleted`);
	} else {

		res.send('Please input username to be deleted')
	}
});

app.listen(port, () => console.log(`Server running at port ${port}`))

